# INTRODUCCIÓN A MONGODB #
MongoDB es un gestor de base de datos orientada a documentos. Esto quiere decir que en lugar de guardar los datos en registros, guarda los datos en documentos. Estos documentos son almacenados en BSON, que es una representación binaria de JSON. 

MongoDB no sigue un esquema como el modelo relacional. Los documentos se guardanen colecciones que se asemejarían a las tablas del modelo relacional. Los documentos no tienen porque tener los mismos campos como vemos en el ejemplo de esta colección a la que llamamos Personas que contiene dos documentos:
<pre>
{
    "Nombre":"Perico",
    "Apellidos":"De los palotes"
    "DNI":"787978J"
},
{
    "Nombre":"John",    
    "DNI":232333233,
    "Telefono":"+7889987987789798"    
}
</pre>

El puerto de escucha es el 27017, la conexión es TCP/IP y la comunicación se realiza mediante texto plano.

MongoDB viene de serie con una consola desde la que podemos ejecutar los distintos comandos. Esta consola está construida sobre JavaScript, por lo que las consultas se realizan utilizando ese lenguaje. 

MongoDB, a través de JSON, puede utilizar los siguientes tipos de datos:

- String: guardados en UTF-8. Van siempre entre dobles comillas.

- Number: números. Al guardarse en BSON pueden ser de tipo byte, int32, int64 o double.

- Boolean: con valor true o false.

- Array: van entre corchetes [] y pueden contener de 1 a N elementos, que pueden ser de cualquiera de los otros tipos.

- Documentos: un documento en formato JSON puede contener otros documentos embebidos que incluyan más documentos o cualquiera de los tipos anteriormente descritos.

- Null.

## EJEMPLO DE DOCUMENTO JSON ##
<pre>
{  
    "People":   
        [  
            {  
                "_id": ObjectId("51c420ba77edcdc3ec709218"),  
                "nombre": "Manuel",  
                "apellidos": "Pérez",  
                "fecha_nacimiento": "1982-03-03",  
                "altura": 1.80,  
                "activo": true,  
                "intereses":["fútbol","tenis"],  
                "tarjeta_credito": null,  
                "dni":  
                    {  
                        "numero":"465464646J",  
                        "caducidad":"2013-10-21"  
                    }  
            },  
            {  
                "_id": ObjectId("51c420ba77ed1dc3ec705289"),  
                "nombre": "Sara",  
                "apellidos": "Ruano",  
                "fecha_nacimiento": "1985-12-03",  
                "altura": 1.65,  
                "activo": false,  
                "intereses":["moda","libros","fotografía","política"],  
                "tarjeta_credito": null  
            }  
        ]  
}
</pre>

## INSTALACIÓN Y TESTEO DE SERVIDOR MONGODB ##
Hacer los siguientes pasos con tu usuario desde el directorio docker/mongodb.

1. Ejecutar el script build_docker.sh para crear la imagen y el container. Sigue los pasos. <pre>$ ./build_docker.sh</pre>

2. Arrancar el container.

3. Consultar estado del container: <pre>$ docker ps</pre>

4. Para iniciar una sesión cliente en el servidor: <pre>$ mongo --host 192.168.2.31 --port 27017</pre>

## REFERENCIAS ##
[Tutorial MongoDB Charlascyclon](http://www.charlascylon.com/mongodb)
