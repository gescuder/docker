# PROYECTO #
Este proyecto se basa en conseguir dockerizar los siguientes servicios:

Apache.

Samba.

Redis.

MongoDB.

Postgres.

HAProxy.

Echo (TCP y UDP).

Time (TCP y UDP).

Daytime (TCP y UDP).

Discard (TCP y UDP).

Chargen (TCP y UDP).

## SERVICIOS DOCKERIZADOS ##
He conseguido dockerizar todos los servicios exceptuando el Chargen y el HAProxy. Con más tiempo podía haberlos sacado pero he preferido dedicar el tiempo en dejar terminados todos los demás.

## QUÉ CONTIENE CADA CARPETA ##
Todas las carpetas contienen como base:

- Dockerfile -> receta para crear la imagen

- build_docker.sh -> script que crea la imagen y el container del servicio

- README.md -> introducción al servicio y guia de instalación y testeo por pasos

- Otros archivos puntuales para cada servicio como pueden ser: archivos de configuración, scripts que son llamados por los dockerfiles, etc.

## PARA EMPEZAR ##
Sigue esta guía para empezar en Docker:

### Instalación de Docker ###

Nos aseguramos de eliminar el paquete docker porque hay un conflicto entre nombres:
<pre># yum -y remove docker</pre>

Instalamos el paquete docker-io:
<pre># yum -y install docker-io</pre>
	
Actualizamos la versión del paquete:
<pre># yum -y update docker-io</pre>

Arrancamos el servicio: 
<pre># systemctl start docker</pre>

Si queremos que el servicio se ponga en marcha a la vez que hacemos boot: 
<pre># systemctl enable docker</pre>

### Añadir permisos a usuarios para utilizar Docker ###
Crear grupo docker:
<pre># groupadd docker</pre>

Cambiamos los permisos al socket que ejecuta el comando docker, ponemos nuestro usuario:
<pre># chown isx46995502:docker /var/run/docker.sock</pre>

Añadimos los usuarios que pueden controlar docker, nuestro usuario en este caso:
<pre># usermod -a -G docker isx46995502</pre>

### Instalar imágenes ###
Descargar la imágen de Fedora 21 de los repositorios de Docker: 
<pre># docker pull fedora:21</pre>

Visualizar las imágenes disponibles:
<pre># docker images</pre>

Buscador de imágenes:
<pre># docker search name</pre>

### Otras instalaciones ###
Puede servir de ayuda instalar el paquete fedora-dockerfiles:
<pre># yum -y install fedora-dockerfiles.x86_64</pre>  
Este paquete contiene una variedad de ejemplos de la comunidad que nos pueden servir para aprender a configurar y arrancar nuestros containers.

### Dockerfiles ###
Un Dockerfile es como una receta. Es un archivo que reconoce Docker y contiene una serie de instrucciones que automatizan la creación de una imagen.

Un ejemplo de Dockerfile (el que utilizaré para el servicio httpd) seria:
<pre>
FROM fedora:21
MAINTAINER "Scott Collier" <scollier@redhat.com>
RUN yum -y install httpd; yum clean all
COPY httpd.conf /etc/httpd/conf/
COPY index.html /var/www/html/
EXPOSE 80
# Simple startup script to avoid some issues observed with container restart
ADD run-apache.sh /run-apache.sh
RUN chmod -v +x /run-apache.sh
ENTRYPOINT ["/run-apache.sh"]
</pre>

### Hello World ###
<pre>$ docker run -i -t --name "Hello_World" fedora echo "Hello World"</pre>

-i -> interactivo, mantiene stdin abierto.
  
-t -> terminal, asigna una pseudo-tty.

--name -> nombre del container. Si no añadimos la orden añade por defecto un nombre del estilo: adjetivo_nombre