# ECHO PROTOCOL #
El echo protocol es un servicio definido en el RFC 862.

# CÓMO FUNCIONA EN TCP? #
Cuando se utiliza vía TCP el servidor trabaja de la siguiente manera:  
1. Server: Escucha en el puerto 7.  
2. Client: Se conecta al puerto 7.  
3. Client: Envía un string.  
4. Server: El servidor retorna el mismo string al cliente.  
5. Client: Recibe la respuesta.  
6. Client: Cierra o no la conexión y/o envía más strings.  
7. Server: Sigue escuchando peticiones.  

El servidor escucha por el puerto 7 conexiones entrantes. Cuando la conexión se establece, el servidor retorna el mismo string que le ha enviado el cliente y se queda a la espera de recibir más conexiones y datos.

# INSTALACIÓN Y TESTEO DE ECHO SERVER (TCP) #
Hacer los siguientes pasos con tu usuario desde el directorio docker/echo-stream.

1. Ejecutar el script build_docker.sh para crear la imagen y el container. Sigue los pasos. <pre>$ ./build_docker.sh</pre>

2. Arrancar el container.

3. Consultar estado del container: <pre>$ docker ps</pre>

4. Para testear que todo funciona correctamente: <pre>$ ncat -t 192.168.2.31 7</pre>

>Cada cosa que escribamos, el servidor echo nos retornará la misma cosa que hemos escrito.  
En caso de que el cliente quiera cerrar la conexión, bastará con apretar: Ctrl + C.  
Comprovamos que podemos conectarnos desde varios clientes a la vez.

### REFERENCIAS ###
[RFC](https://tools.ietf.org/html/rfc862)
