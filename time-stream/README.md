# TIME PROTOCOL #
El time protocol es un protocolo de red definido en el RFC 868.

## COMO FUNCIONA EN TCP? ##
Cuando se utiliza vía TCP el servidor trabaja de la siguiente manera:  
Server: Escucha en el puerto 37.  
Client: Se conecta al puerto 37.  
Server: Envia el tiempo como un número binario de 32 bits.  
Client: Recibe el tiempo.  
Client: Cierra la conexión.  
Server: Sigue escuchando peticiones.  

El servidor escucha por el puerto 37 conexiones entrantes. Cuando la conexión se establece, el servidor retorna el tiempo como un número binario de 32 bits y se queda a la espera de recibir más conexiones.  
La fecha se envía en segundos. El resultado son los segundos que han pasado desde las 00:00 del 1 de Enero de 1970 GMT.

## INSTALACIÓN Y TESTEO DE SERVIDOR TIME (TCP) ##
Hacer los siguientes pasos con tu usuario desde el directorio docker/time-stream.

1. Ejecutar el script build_docker.sh para crear la imagen y el container. Sigue los pasos. <pre>$ ./build_docker.sh</pre>

2. Arrancar el container.

3. Consultar estado del container: <pre># docker ps</pre> 

4. Para testear que todo funciona correctamente: <pre># ncat -t --recv-only 192.168.2.31 37</pre>
>Esto nos devolverá la fecha en segundos desde epoch.   
El servidor cerrará automáticamente la conexión con el cliente porque el cliente indica que solo quiere recibir datos.  
Es importante poner --recv-only para que el servidor cierre la conexión con el cliente una vez le envía los datos. Si no aparece esta opción, el cliente cerrará la conexión con Ctrl + C.

### REFERENCIAS ###
[RFC](https://tools.ietf.org/html/rfc868)
