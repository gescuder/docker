#!/bin/sh
# usage ./build_docker.sh
echo "---------- CREANDO CONTAINER PARA EL SERVIDOR DISCARD (TCP) ----------"

IMG_NAME=${1:-fedora/discard-stream}
CTN_NAME=${2:-fedora_discard-stream-ctn}

echo "Nombre de imagen: $IMG_NAME"
echo "Nombre de container: $CTN_NAME"

docker build --rm -t $IMG_NAME .
docker create --name $CTN_NAME -i -t -p 192.168.2.31:9:9/tcp $IMG_NAME

exit 0
