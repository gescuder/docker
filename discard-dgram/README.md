# DISCARD PROTOCOL #
El discard protocol es un servicio definido en el RFC 863.

## CÓMO FUNCIONA EN UDP? ##
Cuando se utiliza vía UDP el servidor trabaja de la siguiente manera:  
Server: Escucha en el puerto 9.  
Client: Se conecta al puerto 9.  
Client: Envía cualquier string.  
Server: Recibe el datagrama con el string pero no hace nada más.  
Client: Sigue o no enviando strings, no recibirá respuesta.  
Client: Cierra la conexión.  
Server: Sigue escuchando peticiones.  

El servidor escucha por el puerto 9 conexiones entrantes. Cuando la conexión se establece, el servidor recibe datagramas del cliente pero no responde y se queda a la espera de recibir más conexiones o datagramas.

## INSTALACIÓN Y TESTEO DE SERVIDOR DISCARD (UDP) ##
Hacer los siguientes pasos con tu usuario desde el directorio docker/discard-stream.

1. Ejecutar el script build_docker.sh para crear la imagen y el container. Sigue los pasos. <pre>$ ./build_docker.sh</pre>

2. Arrancar el container.

3. Consultar estado del container: <pre># docker ps</pre>

4. Para testear que todo funciona correctamente: <pre># ncat -u 192.168.2.31 9</pre>
>Cada string que enviemos no tendrá respuesta del servidor.  
Apretamos Ctrl + C para cerrar la conexión.

### REFERENCIAS ###
[RFC](https://tools.ietf.org/html/rfc863)
