# INTRODUCCIÓN A REDIS #
Las siglas REDIS corresponden a Remote Dictionary Server.

Redis es un motor de base de datos libre de tipo clave-valor (key-value) persistententes que residen en memoria ram y posteriormente vuelca el conjunto de datos almacenados al disco duro. Redis es cliente/servidor por lo que levanta su servicio y responde peticiones, cuenta con interfaz de red lo cual hace posible conectar clientes o nodos desde otros host.

Redis es key/value esto significa que solo entiende Claves y Valores y maneja las siguientes estructuras de datos:

- Cadenas: es la estructura más básica. Independientemente al nombre de la clave, el valor puede ser cualquier cosa.
 
- Listas: las listas permiten almacenar y manipular un conjunto de valores dada una clave concreta. Puedes añadir valores a la lista, recuperar el primer o el último valor y manipular valores de una posición concreta. Las listas mantienen un orden y son eficientes al realizar operaciones basadas en su índice.

- Hashes: los hashes son como las cadenas de texto. La diferencia más importante es que los hashes incluyen un nivel extra de indirección. Podemos dar valor a varios campos a la vez, obtener varios campos a la vez, recuperar todos los campos y sus valores, mostrar todos los campos o borrar un campo específico.

- Conjuntos: los conjuntos se emplean para almacenar valores únicos y facilitan un número de operaciones útiles para tratar con ellos, como las uniones. Los conjuntos no mantienen orden pero brindan operaciones eficientes basándose en los valores.

- Conjuntos ordenados: la última y más poderosa estructura de datos son los conjuntos ordenados. Si los hashes son como las cadenas de texto pero con campos, entonces los conjuntos ordenados son como los conjuntos pero con una puntuación. La puntuación facilita la ordenación y la capacidad de establecer un ranking.

De igual manera Redis provee mecanismos para manipular y consultar estas estructuras de datos. Al no pertenecer al mundo relacional automáticamente entra en el grupo de bases de datos libres de tipo NoSQL (Not Only SQL).

Las claves identifican el conjunto de datos. Una clave tiene este aspect: users:guillem.  
Los valores representan los datos que estan relacionados con la clave.  
Redis trata los valores como array de bytes sin importar el tipo de dato.

Las bases de datos se identifican mediante números, siendo la base de datos por defecto el número 0. Si queremos movernos entre bases de datos, escribiremos el comando SELECT x, siendo x un número entero. De forma automática saltaremos a la base de datos deseada, siempre podemos saltar a la base de datos que queramos.

Con respecto a la persistencia, Redis va creando fotos de la base de datos que almacena en disco en base a cúantas claves han cambiado. Como alternativa, Redis puede funcionar de manera diferencial, es decir, cada vez que una clave cambia se actualiza un fichero con únicamente el diferencial en disco.

El puerto de escucha es el 6379, la conexión es TCP/IP y la comunicación se realiza mediante texto plano.

Tenemos una guía de [comandos](http://www.redis.io/commands) a utilizar en la sesión interactiva con el servidor.

Se está aplicando en páginas web que necesitan manejar grandes cantidades de datos como twitter, instagram, 	

# INSTALACIÓN Y TESTEO DE SERVIDOR REDIS #
Hacer los siguientes pasos con tu usuario desde el directorio docker/redis.

1. Ejecutar el script build_docker.sh para crear la imagen y el container. Sigue los pasos. <pre>$ ./build_docker.sh</pre>

2. Arrancar el container.

3. Consultar estado del container: <pre>$ docker ps</pre>

4. Descargar en local el paquete redis: <pre># yum -y install redis</pre>

4. Para iniciar una sesión cliente en el servidor: <pre>$ redis-cli -h 192.168.2.31 -p 6379</pre>

Vamos a seguir este [tutorial](http://try.redis.io/) realizando las pruebas en la propia interfaz web. Todo lo que hagamos en esta interfaz es exactamente igual a lo que nos permite la sesión cliente que hemos abierto anteriormente pero vamos a seguir el tutorial en la web.

# REFERENCIAS #
[Web oficial de Redis](http://www.redis.io/)  
[Guía Raul Exposito](http://raulexposito.com/documentos/redis/)
