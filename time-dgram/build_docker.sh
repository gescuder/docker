#!/bin/sh
# usage ./build_docker.sh
echo "---------- CREANDO CONTAINER PARA EL SERVIDOR TIME (UDP) ----------"

IMG_NAME=${1:-fedora/time-dgram}
CTN_NAME=${2:-fedora_time-dgram-ctn}

echo "Nombre de imagen: $IMG_NAME"
echo "Nombre de container: $CTN_NAME"

docker build --rm -t $IMG_NAME .
docker create --name $CTN_NAME -i -t -p 192.168.2.31:37:37/udp $IMG_NAME

exit 0
