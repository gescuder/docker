# TIME PROTOCOL #
El time protocol es un servicio definido en el RFC 868. 

## CÓMO FUNCIONA EN UDP? ##
Cuando se utiliza vía UDP el servidor trabaja de la siguiente manera:  
Server: Escucha en el puerto 37.  
Client: Se conecta al puerto 37.  
Client: Envía un salto de linea.  
Server: Recibe el datagrama vacío.  
Server: Envia el tiempo como un número binario de 32 bits.  
Client: Recibe el tiempo.  
Client: Cierra o no la conexión, si sigue enviando saltos de linea, el servidor vuelve a enviar el tiempo.  
Server: Sigue escuchando peticiones.  

El servidor escucha por el puerto 37 conexiones entrantes. Cuando la conexión se establece, el servidor retorna el tiempo como un número binario de 32 bits y se queda a la espera de recibir más conexiones.  
La fecha se envía en segundos. El resultado son los segundos que han pasado desde las 00:00 del 1 de Enero de 1970 GMT.

## INSTALACIÓN Y TESTEO DE SERVIDOR TIME (UDP) ##
Hacer los siguientes pasos con tu usuario desde el directorio docker/time-stream.

1. Ejecutar el script build_docker.sh para crear la imagen y el container. Sigue los pasos. <pre>$ ./build_docker.sh</pre>

2. Arrancar el container.

3. Consultar estado del container: <pre># docker ps</pre> 

4. Para testear que todo funciona correctamente: <pre># ncat -u 192.168.2.31 37</pre>
>Esto nos devolverá la fecha en segundos desde epoch.  
El cliente cerrará la conexión apretando Ctrl + C.

### REFERENCIAS ###
[RFC](https://tools.ietf.org/html/rfc868)
