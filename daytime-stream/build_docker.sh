#!/bin/sh
# usage ./build_docker.sh
echo "---------- CREANDO CONTAINER PARA EL SERVIDOR DAYTIME (TCP) ----------"

IMG_NAME=${1:-fedora/daytime-stream}
CTN_NAME=${2:-fedora_daytime-stream-ctn}

echo "Nombre de imagen: $IMG_NAME"
echo "Nombre de container: $CTN_NAME"

docker build --rm -t $IMG_NAME .
docker create --name $CTN_NAME -i -t -p 192.168.2.31:13:13/tcp $IMG_NAME

exit 0
