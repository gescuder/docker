# DAYTIME PROTOCOL #
El daytime protocol es un servicio definido en el RFC 867.

## CÓMO FUNCIONA EN TCP? ##
Cuando se utiliza vía TCP el servidor trabaja de la siguiente manera:  
Server: Escucha en el puerto 13.  
Client: Se conecta al puerto 13.  
Server: Envia la fecha actual en formato UNIX humano.  
Client: Recibe la fecha y el servidor le cierra automáticamente la conexión.  
Server: Sigue escuchando peticiones.  

El servidor escucha por el puerto 13 conexiones entrantes. Cuando la conexión se establece, el servidor envía la fecha en formato UNIX humano y le cierra al cliente la conexión y se queda a la espera de recibir más conexiones.

## INSTALACIÓN Y TESTEO DE SERVIDOR DAYTIME (TCP) ##
Hacer los siguientes pasos con tu usuario desde el directorio docker/daytime-stream.

1. Ejecutar el script build_docker.sh para crear la imagen y el container. Sigue los pasos. <pre>$ ./build_docker.sh</pre>

2. Arrancar el container.

3. Consultar estado del container: <pre># docker ps</pre>

4. Para testear que todo funciona correctamente: <pre># ncat -t --recv-only 192.168.2.31 13</pre>
>Esto nos devolverá la fecha actual en formato UNIX humano.  
El servidor cierra la conexión porque el cliente no envía nada.  
Es importante utilizar --recv-only para informar al servidor de que no enviaremos datos. Sin esta orden el cliente deberá cerrar la conexión con Ctrl + C.

### REFERENCIAS ###
[RFC](https://tools.ietf.org/html/rfc867)
