#!/usr/bin/python
#-*- coding: utf-8-*-q

# CHARGEN server program
import socket, sys

HOST = ''
PORT = 19

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# prevenció per a que no quedi el address en time_wait quan finalitzem el socket
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

s.bind((HOST, PORT))

s.listen(1)

fileIn=open("texto.txt","r")

while True:
        conn, addr = s.accept()
        while True:
                for line in fileIn:
                        conn.sendall(line)
                fileIn.seek(0)
s.close()
sys.exit(0)
