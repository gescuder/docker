# CHARGEN PROTOCOL #
El chargen protocol es un servicio definido en el RFC 864.

## CÓMO FUNCIONA EN TCP? ##
Cuando se utiliza vía TCP el servidor trabaja de la siguiente manera:  
Server: Escucha en el puerto 19.  
Client: Se conecta al puerto 19.  
Server: El servidor envía carácteres ASCII sin parar.  
Client: Recibe el stream de carácteres.  
Client: Cierra o no la conexión.  
Server: Sigue escuchando peticiones.  

El servidor escucha por el puerto 19 conexiones entrantes. Cuando la conexión se establece, el servidor envía carácteres ASCII sin parar hasta que el cliente cierra la conexión o no y se queda a la espera de recibir más conexiones.  
Los carácteres enviados pueden ser cualquiera, pero es recomendado seguir un patrón reconozible, yo sigo el patrón que véis en el archivo texto.txt.

## INSTALACIÓN Y TESTEO DE CHARGEN SERVER (TCP) ##
Hacer los siguientes pasos con tu usuario desde el directorio docker/chargen-stream.

1. Ejecutar el script build_docker.sh para crear la imagen y el container. Sigue los pasos. <pre>$ ./build_docker.sh</pre>

2. Arrancar el container.

3. Consultar estado del container: <pre>$ docker ps</pre>

4. Para testear que todo funciona correctamente: <pre>$ ncat -t --recv-only 192.168.2.31 19</pre>
>El cliente va a recibir un stream de carácteres que puede realentizar el ordenador.  
Los generadores de carácteres suelen utilizarse para hacer ataques de denegación de servicio ya que envías muchos paquetes sin parar.   
En caso de que el cliente quiera cerrar la conexión, bastará con apretar: Ctrl + C.  

### REFERENCIAS ###
[RFC](https://tools.ietf.org/html/rfc864)
