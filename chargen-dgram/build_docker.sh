#!/bin/sh
# usage ./build_docker.sh
echo "---------- CREANDO CONTAINER PARA EL SERVIDOR CHARGEN (UDP) ----------"

IMG_NAME=${1:-fedora/chargen-dgram}
CTN_NAME=${2:-fedora/chargen-dgram-ctn}

echo "Nombre de imagen: $IMG_NAME"
echo "Nombre de container: $CTN_NAME"

docker build --rm -t $IMG_NAME .
docker create --name $CONTAINER_NAME -i -t -p 192.168.2.31:19:19/udp $IMG_NAME

exit 0
