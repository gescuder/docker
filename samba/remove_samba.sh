#!/bin/sh

echo "Stopping containers..."
docker stop fedora_samba_base-ctn
docker stop fedora_smbd-ctn
docker stop fedora_nmbd-ctn

echo "Removing containers..."
docker rm fedora_samba_base-ctn
docker rm fedora_smbd-ctn
docker rm fedora_nmbd-ctn

echo "Removing images..."
docker rmi -f fedora/samba_base
docker rmi -f fedora/smbd
docker rmi -f fedora/nmbd

exit 0
