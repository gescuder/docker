# INTRODUCCIÓN A SAMBA #
Samba es una implementación libre del protocolo de archivos compartidos de Microsoft Windows (antiguamente llamado SMB, renombrado recientemente a CIFS) para sistemas de tipo UNIX. De esta forma, es posible que computadoras con GNU/Linux, Mac OS X o Unix en general se vean como servidores o actúen como clientes en redes de Windows. Samba también permite validar usuarios haciendo de Controlador Principal de Dominio (PDC), como miembro de dominio e incluso como un dominio Active Directory para redes basadas en Windows; aparte de ser capaz de servir colas de impresión, directorios compartidos y autentificar con su propio archivo de usuarios.

Samba configura directorios Unix y GNU/Linux (incluyendo sus subdirectorios) como recursos para compartir a través de la red. Para los usuarios de Microsoft Windows, estos recursos aparecen como carpetas normales de red. Los usuarios de GNU/Linux pueden montar en sus sistemas de archivos estas unidades de red como si fueran dispositivos locales, o utilizar la orden smbclient para conectarse a ellas muy al estilo del cliente de la línea de órdenes ftp. Cada directorio puede tener diferentes permisos de acceso sobrepuestos a las protecciones del sistema de archivos que se esté usando en GNU/Linux.

## CÓMO FUNCIONA? ##
El Dockerfile de samba:  
- instala los paquetes de samba que necesitamos.

- crea un directorio que será el que compartiremos con un archivo que generaremos con un ls.

- copia el fichero de configuración de samba al directorio /etc/samba/.

- crea un usuario de prueba y le añade un smbpasswd.

Mediante el script **build_docker.sh** se van a crear 3 imágenes y 3 containers:  
- una para la base de samba, la cual tendrá la configuración de samba, el directorio que publicamos y los usuarios,

- otra para arrancar el servicio smbd

- y otra para arrancar el servicio nmbd.

Las imágenes de smbd y nmbd se ejecutan en base a la imagen base de samba creada primeramente.

## Y SI QUIERO AÑADIR MÁS USUARIOS O MODIFICAR LA CONFIGURACIÓN? ##
Para eso deberíamos eliminar los 3 containers e imágenes que hemos creado de samba_base, smbd y nmbd.  
El caso es que las imágenes/containers nmbd y smbd utilizan como base la imagen de samba base, por lo tanto si realizamos cambios en esta imagen, estos no se veran reflejados en las imágenes/containers de nmbd y smbd hasta que no volvamos a ejecutar de nuevo el **build_docker.sh**.
Así que para facilitar este trabajo, dejo el script **remove_samba.sh** que ejecutaremos antes de hacer cambios que elimina cualquier rastro de nuestro trabajo anterior en samba.

Podría ser todo más reusable, haciendo un script que crea usuarios en la imagen base de samba, que las imágenes de smbd y nmbd tuvieran constancia de esos cambios de la imagen base, pero por tiempo hago una práctica sencilla que funciona.

## TESTEO DE SERVIDOR SAMBA ##
Hacer los siguientes pasos desde docker/samba
1. Ejecutar el script **./build_docker.sh**.

2. Arrancamos los containers **fedora_smbd-ctn** y **fedora_nmbd-ctn**.

3. Desde nuestra sesión de usuario observamos los recursos compartidos. <pre>$ smbclient -L //192.168.2.31 -U pepito</pre>
>El usuario que hemos creado se llama "pepito" con password "jupiter".

4. Comprobado que vemos los recursos compartidos, vamos a montar en el sistema el recurso compartido.  
>Para ello es necesario tener instalado el paquete **cifs-utils** y montar desde una sesión de root.  
<pre># mount -t cifs //192.168.2.31/prueba /mnt -o user=pepito,password=jupiter</pre>
>prueba es el recurso que compartimos.

5. Probamos de entrar directamente al recurso con smbclient. <pre>$ smbclient //192.168.2.31/prueba -U pepito</pre>
>El usuario que hemos creado se llama "pepito con password "jupiter".
