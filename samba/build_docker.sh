#!/bin/sh
# usage ./build_docker.sh [imagen_samba_base..container_samba_base..imagen_smbd..container_smbd..imagen_nmbd..container_nmbd]

echo "---------- CREANDO CONTAINER BASE DE SAMBA ----------"

IMG_NAME=${1:-fedora/samba_base}
CTN_NAME=${2:-fedora_samba_base-ctn}

echo "Nombre de imagen: $IMG_NAME"
echo "Nombre de container: $CTN_NAME"

docker build --rm -t $IMG_NAME .
docker create --name $CTN_NAME \
	-i -t \
	$IMG_NAME \
	/bin/bash

echo "---------- CREANDO CONTAINER DE SMBD ----------"
IMG_NAME=${3:-fedora/smbd}
CTN_NAME=${4:-fedora_smbd-ctn}

echo "Nombre de imagen: $IMG_NAME"
echo "Nombre de container: $CTN_NAME"

docker build --rm -t $IMG_NAME smb/.
docker create --name $CTN_NAME \
	-i -t \
	-p 192.168.2.31:139:139 \
	-p 192.168.2.31:135:135 \
	-p 192.168.2.31:445:445 \
	$IMG_NAME

echo "---------- CREANDO CONTAINER DE NMBD ----------"

IMG_NAME=${5:-fedora/nmbd}
CTN_NAME=${6:-fedora_nmbd-ctn}

echo "Nombre de imagen: $IMG_NAME"
echo "Nombre de container: $CTN_NAME"

docker build --rm -t $IMG_NAME nmb/.
docker create --name $CTN_NAME \
	-i -t \
	-p 192.168.2.31:137:137/udp \
	-p 192.168.2.31:138:138/udp \
	$IMG_NAME

exit 0
