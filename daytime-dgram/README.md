# DAYTIME PROTOCOL #
El daytime protocol es un servicio definido en el RFC 867.

## CÓMO FUNCIONA EN UDP? ##
Cuando se utiliza vía UDP el servidor trabaja de la siguiente manera:  
Server: Escucha en el puerto 13.  
Client: Se conecta al puerto 13.  
Client: Envía un salto de linea.  
Server: Recibe el datagrama vacío.  
Server: Envía la fecha actual en formato UNIX humano.  
Client: Recibe la fecha actual y si vuelve a enviar un salto de linea volverá a recibir la fecha actual.  
Client: Cierra o no la conexión.  
Server: Sigue escuchando peticiones.  

El servidor escucha por el puerto 13 conexiones entrantes. Cuando la conexión se establece, el servidor envía la fecha en formato UNIX humano, el cliente cierra o no la conexión, y el servidor se queda a la espera de recibir más conexiones o datagramas.

## INSTALACIÓN Y TESTEO DE SERVIDOR DAYTIME (UDP) ##
Hacer los siguientes pasos con tu usuario desde el directorio docker/daytime-dgram.

1. Ejecutar el script build_docker.sh para crear la imagen y el container. Sigue los pasos. <pre>$ ./build_docker.sh</pre>

2. Arrancar el container.

3. Consultar estado del container: <pre># docker ps</pre>

4. Para testear que todo funciona correctamente: <pre># ncat -u 192.168.2.31 13</pre>
>Esto nos devolverá la fecha actual en formato UNIX humano.  
Apretaremos Ctrl + C para cerrar la conexión.

### REFERENCIAS ###
[RFC](https://tools.ietf.org/html/rfc867)
