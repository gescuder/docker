#!/bin/sh
# usage ./build_docker.sh
echo "---------- CREANDO CONTAINER PARA EL SERVIDOR POSTGRESQL ----------"

IMG_NAME=${1:-fedora/postgres}
CTN_NAME=${2:-fedora_postgres-ctn}

echo "Nombre de imagen: $IMG_NAME"
echo "Nombre de container: $CTN_NAME"

docker build --rm -t $IMG_NAME .
docker create --name $CTN_NAME -i -t -p 192.168.2.31:5432:5432 $IMG_NAME

exit 0
