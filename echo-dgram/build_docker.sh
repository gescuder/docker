#!/bin/sh
# usage ./build_docker.sh
echo "---------- CREANDO CONTAINER PARA EL SERVIDOR ECHO (UDP) ----------"

IMG_NAME=${1:-fedora/echo-dgram}
CTN_NAME=${2:-fedora_echo-dgram-ctn}

echo "Nombre de imagen: $IMG_NAME"
echo "Nombre de container: $CTN_NAME"

docker build --rm -t $IMG_NAME .
docker create --name $CTN_NAME -i -t -p 192.168.2.31:7:7/udp $IMG_NAME

exit 0
