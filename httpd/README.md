# APACHE #
El servidor Apache es un servidor web HTTP de código abierto para la creación de páginas y servicios web.

## COMO FUNCIONA APACHE? ##
El servidor web está alojado en una máquina que cuenta con conexión a Internet. Dicho servidor se encuentra a la espera de recibir peticiones HTTP a las que responde enviando código HTML mediante transferencia de datos.

## INSTALACIÓN Y TESTEO DE SERVIDOR APACHE ##
Hacer los siguientes pasos con tu usuario desde el directorio docker/httpd.

1. Ejecutar el script build_docker.sh para crear la imagen y el container. Sigue los pasos. <pre>$ ./build_docker.sh</pre>

2. Arrancar el container.

3. Consultar estado del container: <pre>$ docker ps</pre> 

4. Para testear que todo funciona correctamente: <pre>$ curl http://192.168.2.31</pre>
>Esto nos devolverá el codigo html o el contenido de nuestro index.html.

