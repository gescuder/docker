#!/bin/sh
# usage ./build_docker.sh [nombre_imagen] [nombre_container]
echo "---------- CREANDO CONTAINER PARA EL SERVIDOR HTTP ----------"

IMG_NAME=${1:-fedora/httpd}
CTN_NAME=${2:-fedora_httpd-ctn}

echo "Nombre de imagen: $IMG_NAME"
echo "Nombre de container: $CTN_NAME"

docker build --rm -t $IMG_NAME .
docker create --name $CTN_NAME -i -t -p 192.168.2.31:80:80 $IMG_NAME

exit 0
